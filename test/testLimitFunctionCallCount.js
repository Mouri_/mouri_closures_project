let limitFunctionCallCount = require('../limitFunctionCallCount.js')

let n = 4;
let func = limitFunctionCallCount(() => {console.log('running');}, n);

func()  // running once
func()  // running twice
func()  // running thrice
func()  // running fourth time
func()  // not running
func()  // not running
let cacheFunction = require('../cacheFunction.js')

let number = 0;
let func = cacheFunction(() => number++ /*this is the cb function*/);

console.log(func(1));   // will run
console.log(func(2));   // will run
console.log(func(3));   // will run
console.log(func(4));   // will run
console.log(func(1));   // will return from cache
console.log(func(4));   // will return from cache
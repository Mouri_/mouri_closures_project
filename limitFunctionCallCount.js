function limitFunctionCallCount(cb, n) {
    if(cb == null){
        return null;
    }
    return function (){
        if(n>0){
            cb();
        }
        n--;
    }
}
module.exports = limitFunctionCallCount;
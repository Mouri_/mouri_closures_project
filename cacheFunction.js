function cacheFunction(cb) {
    let cache = {}
    return function(argument){
        if(cache.hasOwnProperty(argument)){
            console.log('cached');
            return cache[argument];
        }
        else{
            cache[argument] = cb();
            console.log('running');
            return cache[argument];
        }
    }
}
module.exports = cacheFunction;